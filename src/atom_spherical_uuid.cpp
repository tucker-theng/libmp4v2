/*
 * The contents of this file are subject to the Mozilla Public
 * License Version 1.1 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of
 * the License at http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS
 * IS" basis, WITHOUT WARRANTY OF ANY KIND, either express or
 * implied. See the License for the specific language governing
 * rights and limitations under the License.
 *
 * The Original Code is MPEG4IP.
 *
 * Contributer has declined to give copyright information, and gives
 * it freely to the world.
 *
 * Contributor(s):
 */

#include "src/impl.h"

namespace mp4v2 {
    namespace impl {

        ///////////////////////////////////////////////////////////////////////////////

        MP4SphericalUUIDAtom::MP4SphericalUUIDAtom(MP4File& file)
            : MP4Atom(file, "uuid")
        {

            // See specification at https://github.com/google/spatial-media/blob/master/docs/spherical-video-rfc.md

            static uint8_t spatial_uuid[] = {
                0xff, 0xcc, 0x82, 0x63, 0xf8, 0x55, 0x4a, 0x93,
                0x88, 0x14, 0x58, 0x7a, 0x02, 0x52, 0x1f, 0xdd
            };
            SetExtendedType(spatial_uuid);

            const char* spherical_xml =
                "<?xml version=\"1.0\"?>"\
                "<rdf:SphericalVideo\n"\
                "xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
                "xmlns:GSpherical=\"http://ns.google.com/videos/1.0/spherical/\">"
                "<GSpherical:Spherical>true</GSpherical:Spherical>"
                "<GSpherical:Stitched>true</GSpherical:Stitched>"
                "<GSpherical:ProjectionType>equirectangular</GSpherical:ProjectionType>"
                "</rdf:SphericalVideo>"; /* len = 347 */

            MP4BytesProperty* data = new MP4BytesProperty(*this, "data");
            data->SetValue((uint8_t*)(spherical_xml), 347);
            AddProperty(data);
        }

        ///////////////////////////////////////////////////////////////////////////////

    }
} // namespace mp4v2::impl
